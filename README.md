## DCMA requests

This project contains the [DCMA requests][dmca-wiki] GitLab Inc. has received.

## Contributions

Unless you're a GitLab team member, you can only contribute to this file. As
always; contributions are greatly appreciated.

For GitLab Team members processing a DCMA request, please clone this project
and run:

```
bin/process path/to/request
```

[dmca-wiki]: https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act
